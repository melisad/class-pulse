const express = require("express");
const app = express();
const PORT = 8080;
const method = {
  GET: {
    routes: ["/", "/hello"]
  },
  POST: {
    routes: []
  },
  PUT: {
    routes: []
  },
  PATCH: {
    routes: []
  },
  DELETE: {
    routes: []
  },
  COPY: {
    routes: []
  },
  HEAD: {
    routes: []
  },
  OPTIONS: {
    routes: []
  },
  LINK: {
    routes: []
  },
  UNLINK: {
    routes: []
  },
  PURGE: {
    routes: []
  },
  LOCK: {
    routes: []
  },
  UNLOCK: {
    routes: []
  },
  PROFIND: {
    routes: []
  },
  VIWE: {
    routes: []
  }
};

app.listen(PORT, () => {
  console.log(`Server is listening on port: ${PORT}`);
});
app.get(method.GET.routes[0], (req, res) => {
  res.send(method);
});
app.get(method.GET.routes[1], (req, res) => {
  res.send("Hello World!");
});
